function montarTabela(parametros, value){
    console.log(parametros);
    console.log(value);
    var html = "<tbody>";

    var param = parametros[0];

    console.log(value[0].param);

    // if (value != null){
    //     for (i=0; i<value.length; i++){
    //         html += "<tr>";
    //         for(j=0; j<parametros.length; j++){
    //             html += "<td>" + value[i].parametros[j] + "</td>";
    //         }
    //         html += "</tr>";    
    //     }
    // }

    html += "</tbody>";

    return html;
}

function voltar(div1, div2, tabela){
    $("#" + div1).show();
    $("#" + div2).hide();
    $("#cabecalho").hide();
    $("#" + tabela).empty();
}

function setSwal(title = '', text = '', type = 'error'){
    swal(title, text, type);
    return;
}

function loader(div1, opcao = 0){
    if (opcao == 0){
        $("#" + div1).hide();
        $("#loader").show();
    } else {
        $("#" + div1).show();
        $("#loader").hide();
    }
}

function stopLoader(div1, div2, tabela, html){
    $("#" + div1).hide();
    $("#loader").hide();    
    $("#" + tabela).append(html);
    $("#" + div2).show();
    $("#cabecalho").show();
}

//Recebe o id do form como parâmetro principal
function formParams(id){
    //Com o id do form, captura todas as inputs 
    var inputs = $("form#" + id + " :input"), i, params = [];
    
    //Laço que percorre as inputs do form
    for (i=0; i<inputs.length; i++){
        //Pega o elemento atual, o tipo e a variável de campo obrigatório
        var el = inputs[i], type = inputs[i].type, required, tipo;  
        
        //Verifica se o campo é obrigatório
        required = $(el).attr("required");
        tipo = $(el).attr("data-type");
        
        //Se o tipo da input for diferente de botão e de arquivo adiciona no array. Senão, faz nada.
        if (type != "button" && type != "file" && tipo != undefined){
            params.push({
                id:    el.id, 
                tipo:  $(el).attr("data-type"), 
                value: el.value, 
                obrigatorio: required === undefined ? "false" : "true"
            });
        }
    }
    
    return params;
}    

function validar(el){
    var type = $(el).attr('data-type');

    window[type](el);
}

function numbers(campo){
    var regex = '/^[0-9]*$/', value = $(campo).val();

    console.log(value.match(regex));

    if (!value.match(regex)){
        setSwal('', 'Campo \'Código\' permite somente números');
        $(campo).val('');
        return false;
    }
}

function text(campo){
    var regex = '/^[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ ]+/';

    if (!$(campo).val().match(regex)){
        setSwal('', 'Digite um CPF válido para pesquisar');
        $(campo).val('');
        return false;
    }
}

function minisite(campo){
    var regex = '/([^a-z0-9_]+)/';

    if (!$(campo).val().match(regex)){
        setSwal('', 'Digite um minisite válido para pesquisar');
        $(campo).val('');
        return false;
    }
}

function cpf(campo){
    var regex = '/^([0-9]{2}[\.]?[0-9]{3}[\.]?[0-9]{3}[\/]?[0-9]{4}[-]?[0-9]{2})|([0-9]{3}[\.]?[0-9]{3}[\.]?[0-9]{3}[-]?[0-9]{2})$/';

    if (!$(campo).val().match(regex)){
        setSwal('', 'Digite um CPF válido para pesquisar');
        $(campo).val('');
        return false;
    }
}

function email(campo){
    var regex = '/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/';

    if (!$(campo).val().match(regex)){
        setSwal('', 'Digite um email válido para pesquisar');
        $(campo).val('');
        return false;
    }    
}

function number_format (number, decimals, decPoint, thousandsSep) {
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number;
    var prec = !isFinite(+decimals) ? 0 : Math.abs(decimals);
    var sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep;
    var dec = (typeof decPoint === 'undefined') ? '.' : decPoint;
    var s = '';
  
    var toFixedFix = function (n, prec) {
        if (('' + n).indexOf('e') === -1) {
            return +(Math.round(n + 'e+' + prec) + 'e-' + prec);
        } else {
            var arr = ('' + n).split('e');
            var sig = '';

            if (+arr[1] + prec > 0) {
                sig = '+';
            }

            return (+(Math.round(+arr[0] + 'e' + sig + (+arr[1] + prec)) + 'e-' + prec)).toFixed(prec);
        }
    }
  
    s = (prec ? toFixedFix(n, prec).toString() : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }

    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
  
    return s.join(dec);    
}

function showAlerts(alerts){
    for (i=0; i<alerts.length; i++){
        $("#" + alerts[i]).show();
    }
}

function hideAlerts(alerts){
    for (i=0; i<alerts.length; i++){
        $("#" + alerts[i]).hide();
    }
}

function dateFormat(data, formato){
    var format = formato.split(/\-\\\//);
}